# MiLongA_CV

Continous validation of MiLongA: Based on test data the quality metrics are checked for congruence


# Overview
MiLongA CV compares MiLongA results on defined test data to the results to a reference version. Differences are summarized in an HTML report

# Usage
1. Download the latest dataset, extract the archive file and create a sample sheet (see MiLongA README)

You may validate the integrity of the test data by comparing the checksums:

```
cd path/testdata
md5sum -c md5sums.txt *.fastq.gz
```

2. Configure MiLongA CV by completing the `config_milonga_cv_dummy.yaml`

Make sure to provide a valid yaml file with the following entries

```yaml
workdir: "" # path to MiLongA_CV repo
milonga_repo: "" # path to MiLongA repo
samples: "" # path to samples.tsv of the test data
fixed_date: "" # CHANGE THIS ONLY IF YOU WANT TO OVERRIDE THE DATE
tag: "" # ADD an institutional tag if desired
reference_version: "v1.0.0" # change this if the reference has changed
```

This needs to be performed only once. Please note the name of your config.yaml file for later usage (see below).

3. Activate your MiLongA conda enviornment, e.g.

```
conda activate milonga
```

Note: Currently it is called milonga_extra

4. Run the MiLongA on a new dataset

```
cd path/Milonga_CV
snakemake --snakefile milonga_cv.smk --configfile config_milonga_cv.yaml -p --cores 10
```

Make sure to specify your configfile. You may also make a dryrun first by appending the `-n` flag to your command.


# Results

The result of MiLongA_CV is an interactive HTML report which is located within the reports folder of your MiLongA test data analysis. It is also available in the folder `MiLongA_CV_Reports`

The report contains several tabs: The overview summarizes the comparison of a new MiLongA version to a reference version based on the test data. A green flag indicates success. Reasons for possible deviations can be deduced from the other tabs.

## Public Results

* [2021-12-13 v1.0.0 Vergleichspraezision_HPC03](/public/milonga_CV_reports/20211213_v1.0.0_Vergleichspraezision_HPC03.html)
* [2021-12-13 v1.0.0 Wiederholbarkeit](/public/milonga_CV_reports/20211213_v1.0.0_Wiederholbarkeit.html)


The analysis parameters can be reviewed in the file [Parameters.R](https://gitlab.com/bfr_bioinformatics/milonga-cv/-/blob/main/scripts/parameters.R).


# Authors:
* Carlus Deneke
