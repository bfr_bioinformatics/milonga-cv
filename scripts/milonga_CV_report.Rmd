---
title: "MiLongA Continous Validation report"
author: "BfR"
date: "Created on updated on `r format(Sys.time(), '%d %B, %Y')`"
output: html_document
---

<style type="text/css">
.main-container {
max-width: 2400px;
margin-left: auto;
margin-right: auto;
}
</style>



```{r setup, include=FALSE}

#author: "BfR"

#output:  html_document
knitr::opts_chunk$set(echo = FALSE)

library(dplyr)
suppressPackageStartupMessages(library(DT)) # datatable 

# source files
#functions_file <- file.path(basedir,"scripts","functions.R")
#parameters_file <- file.path(basedir,"scripts","parameters.R")


setwd("/cephfs/abteilung4/Projects_NGS/Akkreditierung_ONT/Milonga_CV") # TODO remove

#TODO need to adapt
functions_file <- file.path("scripts","functions.R")
parameters_file <- file.path("scripts","parameters.R")



stopifnot(file.exists(functions_file))
stopifnot(file.exists(parameters_file))

source(functions_file)
source(parameters_file)


executor <- Sys.info()["user"]

```


---
author: `r paste0(executor)`
---

```{r HelperFunctions, echo = FALSE}

currentScript <- function() {
  .getSourcedScriptName <- function() {
    for (i in sys.nframe():1) {
      x <- sys.frame(i)$ofile
      if (!is.null(x)) {return(normalizePath(x))}
    }
  }
  if (is.null(.getSourcedScriptName())) {
    return(normalizePath(rstudioapi::getActiveDocumentContext()$path))
  } else {
    .getSourcedScriptName()
  }
}

```



```{r define_data, include=FALSE}

# basedir <- "/cephfs/abteilung4/Projects_NGS/Assembly_pipeline/continuous_integration" # defined above




# live
#debug <- F


if(exists("snakemake")){
# snakemake parameters  
  basedir <- snakemake@config$workdir
  comparison_name <-snakemake@params[["name"]]
  comparison_version <- snakemake@params[["version"]]
  comparison_date <- snakemake@params[["date"]]
  reference_version <- snakemake@params[["reference_version"]]
  
  setwd(dir = snakemake@config$workdir)
  
} else {
  # test
  warning("in debug mode")
  basedir <- "/cephfs/abteilung4/Projects_NGS/Akkreditierung_ONT/Milonga_CV"
  #comparison_name <-params$name
  comparison_name <- "20211213_v1.0.0_Wiederholbarkeit" #"testdata_results" # TODO read in as parameter
  comparison_version <- strsplit(comparison_name,"_")[[1]][2]
  #reference_hash = "Unknown"
  comparison_date <- strsplit(comparison_name,"_")[[1]][1]
  reference_version = "v1.0.0"
} 




reference_dir <- file.path(basedir,"reference")
newdata_dir <- file.path(basedir,comparison_name)

#reference_dir <- newdata_dir # TODO
#warning("Test mode, waiting for test dir")

stopifnot(dir.exists(newdata_dir))

# define data files ---
repodir <- basedir
pass_image <- file.path(repodir,"scripts","pass.png")
fail_image <- file.path(repodir,"scripts","fail.png")


# software_versions.tsv  summary_assembly.tsv  summary_qc.tsv


reference_config.file <- file.path(reference_dir,"config.yaml")
reference_softwareversions.file <- file.path(reference_dir,"reports","software_versions.tsv")
reference_report.file <- file.path(reference_dir,"reports","summary_assembly.tsv")
reference_qc.file <- file.path(reference_dir,"reports","summary_qc.tsv")
#reference_fastp.file <- file.path(reference_dir,"reports","summary_fastp.tsv")

newdata_config.file <- file.path(newdata_dir,"config.yaml")
newdata_softwareversions.file <- file.path(newdata_dir,"reports","software_versions.tsv")
newdata_report.file <- file.path(newdata_dir,"reports","summary_assembly.tsv")
newdata_qc.file <- file.path(newdata_dir,"reports","summary_qc.tsv")
#newdata_fastp.file <- file.path(newdata_dir,"reports","summary_fastp.tsv")

# check existence
stopifnot(file.exists(reference_config.file))
stopifnot(file.exists(reference_softwareversions.file))
stopifnot(file.exists(reference_report.file))
stopifnot(file.exists(reference_qc.file))
#stopifnot(file.exists(reference_fastp.file))
stopifnot(file.exists(newdata_config.file))
stopifnot(file.exists(newdata_softwareversions.file))
stopifnot(file.exists(newdata_report.file))
stopifnot(file.exists(newdata_qc.file))
#stopifnot(file.exists(newdata_fastp.file))

# define theshold data ---

# load data ---
reference_config <- yaml::read_yaml(reference_config.file) %>% unlist()
reference_softwareversions <- read.delim(reference_softwareversions.file,stringsAsFactors=F, sep = " ")
reference_report <- read.delim(reference_report.file,stringsAsFactors=F, check.names = F)
reference_qc <- read.delim(reference_qc.file,stringsAsFactors=F, check.names = F)
# reference_fastp <- read.delim(reference_fastp.file,stringsAsFactors=F, check.names = F)
newdata_config <- yaml::read_yaml(newdata_config.file) %>% unlist()
newdata_softwareversions <- read.delim(newdata_softwareversions.file,stringsAsFactors=F, sep = " ")
newdata_report <- read.delim(newdata_report.file,stringsAsFactors=F, check.names = F)
newdata_qc <- read.delim(newdata_qc.file,stringsAsFactors=F, check.names = F)
# newdata_fastp <- read.delim(newdata_fastp.file,stringsAsFactors=F, check.names = F)

reference_hash <- digest::digest(reference_report.file,"sha256")

```



```{r compute_QC decisions}




# qc decisions ----
#qc_decisions_all_equal <- all.equal(reference_qc[,1:6] ,newdata_qc[,1:6])
#qc_decisions_unchanged <- sum(reference_qc[,1:6] == newdata_qc[,1:6], na.rm=T)
#count_qc_decisions <- length(unlist(reference_qc))



# new way

qc_decisions_all_equal <- all.equal(reference_qc[,"all_pass"] ,newdata_qc[,"all_pass"])

reference_qc_counts <- colSums(reference_qc[,3:5])
newdata_qc_qc_counts <- colSums(newdata_qc[,3:5])

qc_decision_counts <- data.frame(
  reference_qc_counts,newdata_qc_qc_counts,
diff = reference_qc_counts - newdata_qc_qc_counts
)

qc_decisions_changed <- sum(qc_decision_counts$diff)
count_qc_decisions <- sum(unlist(reference_qc[,3:5]))
qc_decisions_unchanged = count_qc_decisions - qc_decisions_changed

```


```{r compute_report_stats}
comparison_report <- bind_rows(reference_report %>% mutate(analysis_type="reference"), newdata_report  %>% mutate(analysis_type="new"))

# STRATEGY:
# loop through species
# keep entries in rows
# add new rows with diff information

# comparison_aquamis_metrics_all <- do.call(rbind,lapply(unique(comparison_report$Species), function(myspecies) {
  # compare_aquamis_metrics (myspecies, comparison_report, equal_cols,diff_notolerance,unchecked_cols,diff_tolerance)  
# }))


comparison_milonga_metrics_all <- do.call(rbind,lapply(unique(comparison_report$sample), function(mysample) {
  compare_milonga_metrics (mysample, "flye",comparison_report, equal_cols,diff_notolerance,unchecked_cols,diff_tolerance)  
}))

# unicycler

comparison_milonga_metrics_allunicycler <- do.call(rbind,lapply(unique(comparison_report$sample), function(mysample) {
  compare_milonga_metrics (mysample, "unicycler",comparison_report, equal_cols,diff_notolerance,unchecked_cols,diff_tolerance)  
}))


## extract summary information ----

### by metric ---

# list of parameters
colswithchecks <- c(colnames(diff_tolerance),equal_cols,diff_notolerance)

summary_by_metric <- do.call(rbind,lapply(colswithchecks, function(mycol) make_columns_summary(mycol,comparison_milonga_metrics_all)))


metric_equal <- summary_by_metric %>% filter(count_equal > 0) %>% select(metric) %>% unlist() %>% as.character()

metric_fails <- summary_by_metric %>% filter(count_fail > 0) %>% select(metric) %>% unlist() %>% as.character()

metric_acceptable <- summary_by_metric %>% filter(count_acceptable > 0) %>% select(metric) %>% unlist() %>% as.character()# %>% paste(.,collapse = ";")



### by species -----



# NON-tidy way:
summary_by_species <- data.frame(comparison_milonga_metrics_all %>% filter(analysis_type == "Check") %>% select(sample),
  is_equal = rowSums(comparison_milonga_metrics_all %>% filter(analysis_type == "Check") == "EQUAL", na.rm = T),
  is_acceptable = rowSums(comparison_milonga_metrics_all %>% filter(analysis_type == "Check") == "ACCEPTABLE", na.rm = T),
  is_fail = rowSums(comparison_milonga_metrics_all %>% filter(analysis_type == "Check") == "FAIL", na.rm = T))


### all summary

report_stats_allsummary <- colSums(summary_by_species[,-1] )

### unicycler

summary_by_metric_unicycler <- do.call(rbind,lapply(colswithchecks, function(mycol) make_columns_summary(mycol,comparison_milonga_metrics_allunicycler)))


metric_equal_unicycler <- summary_by_metric_unicycler %>% filter(count_equal > 0) %>% select(metric) %>% unlist() %>% as.character()


metric_fails_unicycler <- summary_by_metric_unicycler %>% filter(count_fail > 0) %>% select(metric) %>% unlist() %>% as.character()

metric_acceptable_unicycler <- summary_by_metric_unicycler %>% filter(count_acceptable > 0) %>% select(metric) %>% unlist() %>% as.character()# %>% paste(.,collapse = ";")


summary_by_species_unicycler <- data.frame(comparison_milonga_metrics_allunicycler %>% filter(analysis_type == "Check") %>% select(sample),
  is_equal = rowSums(comparison_milonga_metrics_allunicycler %>% filter(analysis_type == "Check") == "EQUAL", na.rm = T),
  is_acceptable = rowSums(comparison_milonga_metrics_allunicycler %>% filter(analysis_type == "Check") == "ACCEPTABLE", na.rm = T),
  is_fail = rowSums(comparison_milonga_metrics_allunicycler %>% filter(analysis_type == "Check") == "FAIL", na.rm = T))


### all summary

report_stats_allsummary_unicycler <- colSums(summary_by_species_unicycler[,-1] )



```


```{r compute_software versions}
# software versions ---
comparison_softwareversions <- merge(reference_softwareversions,newdata_softwareversions,by="Software", suffixes = c(".reference",".new"))
comparison_softwareversions$Version_change <- ifelse(comparison_softwareversions$Version.reference == comparison_softwareversions$Version.new,"=","CHANGE")

#TODO find if higher-lower, minor,major change

# summarize
count_softwares <- length(comparison_softwareversions$Version_change)
softwareversions_unchanged <- sum(comparison_softwareversions$Version_change == "=")
softwareversions_unchanged_relative <- mean(comparison_softwareversions$Version_change == "=")

```

```{r decide_all_pass}

if(report_stats_allsummary["is_fail"] == 0 & report_stats_allsummary_unicycler["is_fail"] == 0){
  all_pass <- T  
} else{
  all_pass <- F
} 

```

# Milonga-CV report {.tabset} 

Continuous validation of MiLongA results compared to reference MiLongA version.

```{r ifelsefail, echo=FALSE, fig.cap="CV decision", out.width = '20%'}

if (all_pass){
  knitr::include_graphics(pass_image)
} else{
  knitr::include_graphics(fail_image)  
} 

```

CV evaluation criteria:

* FAIL if any flye or unicycler comparison metric deviates from accepted tolerance
* PASS if no flye or unicycler comparison metric deviates from accepted tolerance

## Overview

* Comparison of version `r comparison_version` against reference
* reference is `r reference_hash`, version `r reference_version`
* Comparison date `r comparison_date`

### Summary QC decisions

* `r qc_decisions_changed` of `r count_qc_decisions` QC decisions are different
* `r qc_decisions_unchanged` of `r count_qc_decisions` QC decisions are unchanged

### Summary Report stats, flye assembly

* `r ifelse(report_stats_allsummary["is_fail"] == 0,"No sample showed any significant different QC value","At least one sample showed a significant different QC value")`
* Identical QC values: `r report_stats_allsummary["is_equal"]`
* Acceptable QC values:`r report_stats_allsummary["is_acceptable"]`
* Different QC values: `r report_stats_allsummary["is_fail"]`

#### By metric

* `r length(metric_fails)` QC Metrics with fails: `r paste(metric_fails, collapse=";")`
* `r length(metric_acceptable)` QC Metrics with acceptable values: `r paste(metric_acceptable, collapse=";")`


#### By strain

```{r print_summary_by_species} 
#* `r length(metric_equal)` QC Metrics are equal
knitr::kable(summary_by_species, row.names = F)
```

### Summary Report stats, unicycler assembly

* `r ifelse(report_stats_allsummary_unicycler["is_fail"] == 0,"No sample showed any significant different QC value","At least one sample showed a significant different QC value")`
* Identical QC values: `r report_stats_allsummary_unicycler["is_equal"]`
* Acceptable QC values:`r report_stats_allsummary_unicycler["is_acceptable"]`
* Different QC values: `r report_stats_allsummary_unicycler["is_fail"]`

#### By metric

* `r length(metric_fails_unicycler)` QC Metrics with fails: `r paste(metric_fails_unicycler, collapse=";")`
* `r length(metric_acceptable_unicycler)` QC Metrics with acceptable values: `r paste(metric_acceptable_unicycler, collapse=";")`


#### By strain

```{r print_summary_by_species_unicycler} 
#* `r length(metric_equal)` QC Metrics are equal
knitr::kable(summary_by_species_unicycler, row.names = F)
```


### Summary Software versions
* `r softwareversions_unchanged` of `r count_softwares` (`r softwareversions_unchanged_relative*100` per cent) of software dependencies are identical (no version change)


```{r Summary Database versions, include=F} 
#### Summary Database versions
#* tbd
```

## QC decisions

`r ifelse(qc_decisions_all_equal,"All QC decisions are equal","Not all QC decisions are equal")``

### QC decision counts

```{r QC decisions counts}

knitr::kable(qc_decision_counts)


```

### QC decisions reference

```{r QC decisions reference}

knitr::kable(reference_qc)

#bind_rows(reference_qc %>% mutate(analysis_type="reference"),newdata_qc  %>% mutate(analysis_type="new"))

```

### QC decisions comparison version

```{r QC decisions newdata}

knitr::kable(newdata_qc)

#bind_rows(reference_qc %>% mutate(analysis_type="reference"),newdata_qc  %>% mutate(analysis_type="new"))


```


## Report stats (flye)


`r ifelse(report_stats_allsummary["is_fail"] == 0,"No sample showed any significant different QC value","At least one sample showed a significant different QC value")`

### By strain

```{r Report stats_1}
knitr::kable(summary_by_species, row.names = F)
```


### All report stats

```{r Report stats_2}

# Datatable
DT::datatable(comparison_milonga_metrics_all,
              filter = 'top',
              rownames= FALSE,
              escape = FALSE,
              extensions = list("ColReorder" = NULL,
                                "Buttons" = NULL,
                                "FixedColumns" = list(leftColumns = 1)),
              options = list(
                dom = 'BRrltpi',
                autoWidth = FALSE,
                scrollX = TRUE,
                #fixedColumns = TRUE,
                lengthMenu = list(
                  c(5, 10, 50, -1),
                  c("5", "10", "50", "All")),
                pageLength = 10,
                ColReorder = TRUE,
                searchHighlight = TRUE,
                scrollCollapse = TRUE,
                buttons = list(
                  list(extend = 'copy',
                       title = NULL,
                       exportOptions = list(columns = ":visible")),
                  list(extend = 'print',
                       title = NULL,
                       exportOptions = list(columns = ":visible")),
                  list(text = 'Download',
                       extend = 'collection',
                       title = NULL,
                       buttons = list(list(extend = 'csv',
                                           title = NULL,
                                           exportOptions = list(columns = ":visible")),
                                      list(extend = 'excel',
                                           title = NULL,
                                           exportOptions = list(columns = ":visible")),
                                      list(extend = 'pdf',
                                           title = NULL,
                                           exportOptions = list(columns = ":visible")))),
                  I('colvis'))
              ))



```

### By Metric

```{R stats_by_metric} 

knitr::kable(summary_by_metric)


```


## Report stats (unicycler)


`r ifelse(report_stats_allsummary_unicycler["is_fail"] == 0,"No sample showed any significant different QC value","At least one sample showed a significant different QC value")`

### By strain

```{r Report stats_1_unicycler}
knitr::kable(summary_by_species_unicycler, row.names = F)
```


### All report stats

```{r Report stats_2_unicycler}

# Datatable
DT::datatable(comparison_milonga_metrics_allunicycler,
              filter = 'top',
              rownames= FALSE,
              escape = FALSE,
              extensions = list("ColReorder" = NULL,
                                "Buttons" = NULL,
                                "FixedColumns" = list(leftColumns = 1)),
              options = list(
                dom = 'BRrltpi',
                autoWidth = FALSE,
                scrollX = TRUE,
                #fixedColumns = TRUE,
                lengthMenu = list(
                  c(5, 10, 50, -1),
                  c("5", "10", "50", "All")),
                pageLength = 10,
                ColReorder = TRUE,
                searchHighlight = TRUE,
                scrollCollapse = TRUE,
                buttons = list(
                  list(extend = 'copy',
                       title = NULL,
                       exportOptions = list(columns = ":visible")),
                  list(extend = 'print',
                       title = NULL,
                       exportOptions = list(columns = ":visible")),
                  list(text = 'Download',
                       extend = 'collection',
                       title = NULL,
                       buttons = list(list(extend = 'csv',
                                           title = NULL,
                                           exportOptions = list(columns = ":visible")),
                                      list(extend = 'excel',
                                           title = NULL,
                                           exportOptions = list(columns = ":visible")),
                                      list(extend = 'pdf',
                                           title = NULL,
                                           exportOptions = list(columns = ":visible")))),
                  I('colvis'))
              ))



```

### By Metric

```{R stats_by_metric_unicycler} 

knitr::kable(summary_by_metric_unicycler)


```



## Software versions

```{r Software versions}
# xxx

knitr::kable(comparison_softwareversions)


## Database versions

#* platon
# * kraken

#```{r Database versions}
# xxx
#```

```


## Pipeline parameters

Comparison of config

```{r Configfile}
# xxx

comparison_config <- bind_rows(reference_config,newdata_config)

comparison_config_t <- data.frame(key = colnames(comparison_config), reference = unlist(comparison_config[1,]) , new = unlist(comparison_config[2,]) )


knitr::kable(comparison_config_t, row.names = F)

```

## CV parameters

Applied parameters for CV

```{r CI_parameters}
# xxx

CI_parameters_content <- scan(file.path(basedir,"scripts","parameters.R"), what = "chatacter", sep = "\n")

print(CI_parameters_content)

```

## Links

Links to original data



```{r Links to data}

link2files <- rbind(
reference_config.file,
reference_softwareversions.file,
reference_report.file,
reference_qc.file,
#reference_fastp.file,
newdata_config.file,
newdata_softwareversions.file,
newdata_report.file,
newdata_qc.file)

knitr::kable(link2files)

```

