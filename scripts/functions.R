# utility functions

# DEFINE FUNCTION FOR COMPARISON:

compare_milonga_metrics <- function(mysample, myassembler = "flye", comparison_report, equal_cols,diff_notolerance,unchecked_cols,diff_tolerance) {
  
  # use Sample_Name as key instead?
  
  # filter data for species
  #comparison_report_species <- comparison_report %>% filter(Species == myspecies)
  comparison_report_species <- comparison_report %>% filter(sample == mysample & assembler == myassembler)
  
  
  cols_with_tolerance <- colnames(diff_tolerance) # DEFINE HERE OR BEFORE
  
  
  # run checks ----------
  
  check1 <- ifelse(comparison_report_species [1,equal_cols] == comparison_report_species [2,equal_cols],"EQUAL","DIFFERENT")
  
  check2 <- as.data.frame(unchecked_cols)
  check2 <- t(data.frame(unchecked_cols))
  check2 <- matrix(data=NA,nrow=1,ncol=length(unchecked_cols))
  colnames(check2) <- unchecked_cols
  check2 <- as.data.frame(check2)
  
  coldiffs_tmp <- abs(comparison_report_species [1,cols_with_tolerance] - comparison_report_species [2,cols_with_tolerance])
  check3 <- ifelse(coldiffs_tmp == 0, "EQUAL",
                   ifelse(coldiffs_tmp <= diff_tolerance, "ACCEPTABLE","DIFFERENT"))
  
  # COLUMNS THAT MAY BE DIFFERENT
  #cols_full_tolerance <- c("Reference" = "","Reference_Accession" = "")
  #check4 <- data.frame(t(cols_full_tolerance))
  
  #check5 <- ifelse(comparison_report_species [1,diff_notolerance] == comparison_report_species [2,diff_notolerance],"EQUAL","DIFFERENT")
  
  
  # combine all indiviual checks and reorder
  #check_species <- cbind(cbind(cbind(cbind(check1,check2),check3),check4),check5)
  #check_species <- do.call(cbind,list(check1,check2,check3,check4,check5)) %>% select(colnames(comparison_report_species))
  check_species <- do.call(cbind,list(check1,check2,check3)) %>% select(colnames(comparison_report_species))
  check_species <- check_species  %>% mutate(sample = mysample) %>% mutate(analysis_type = "Check", assembler = myassembler)
  
  # add row showing the numerical differences
  othercols <- setdiff(colnames(comparison_report_species),cols_with_tolerance)
  otherdifs <- matrix(data = NA,nrow=1,ncol=length(othercols), dimnames = list("1",othercols))
  
  coldiffs <- cbind(coldiffs_tmp,otherdifs)  %>% mutate(analysis_type = "Diff") %>% mutate(sample = mysample, assembler = myassembler) %>% select(colnames(comparison_report_species))
  
  
  
  comparison_report_species_extended <- rbind(rbind(comparison_report_species,check_species),coldiffs)
  
  
  
  #return this
  #comparison_report_species_extended <- comparison_report_species_extended #%>% relocate(Species,analysis_type,.before = Sample_Name)
  
  
  return(comparison_report_species_extended)
  
} # end function

# -----------------------------

make_columns_summary <- function(mycol,comparison_aquamis_metrics_all){ 
  
  myvalues <- comparison_aquamis_metrics_all %>% filter(analysis_type == "Check") %>% select(all_of(mycol))
  
  summary_mycol <- data.frame(
    metric = mycol,
    count_equal = sum(unlist(myvalues) == "EQUAL"),
    count_acceptable = sum(unlist(myvalues) == "ACCEPTABLE"),
    count_fail = sum(unlist(myvalues) == "FAIL")
  )
  
  return(summary_mycol)
}




# COLUMNS THAT HAVE NUMERICAL TOLERANCE
# FAIL THRESHOLD


#comparison_report <- bind_rows(reference_report %>% mutate(analysis_type="reference"), newdata_report  %>% mutate(analysis_type="new")) #%>% filter(Species == myspecies) %>% t() %>% as.data.frame()
#colnames(comparison_report) <- c("Value_reference","Value_new")
#comparison_report$Value_reference == comparison_report$Value_new
#class(comparison_report$Value_reference)

# value equality
# value difference (if numeric)
# value relative difference (if numeric)
# value defined margin
# value difference within margin: T/F

