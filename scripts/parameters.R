# DEFINITIONS ---


# NUmerical columns with tolerance
# ----

equal_cols <- c(
"largest_contig_circular",
"taxonmic_classification")


diff_notolerance <- c()


unchecked_cols <- c(
"sample",
"analysis_type",
"assembler",
"amr_genes",
"inc_types")                 


diff_tolerance <- data.frame(
"contigs" = 2,
"contigs_l5kbp" = 2,
"contigs_l20kbp" = 2,
"total_length" = 1000,
"circular_contigs" = 2,           
"plasmid_contigs" = 2,           
"plasmids_cumlength" = 1000,        
"largest_contig_size" = 1000,
"amr_genes_count" = 2,
"amr_genes_count_plasmid" = 2,
"amr_genes_count_chromosome" = 2,
"genome_completeness" = 2,
"genome_contamination" = 2,
"strain_heterogeneity" = 2,
"taxonmic_purity" = 0.02,
"misalignment_length" = 1000
)

cols_with_tolerance <- colnames(diff_tolerance) # DEFINE HERE OR LATER

# from aquamis ----

# diff_tolerance <- data.frame(
#   "n_Reads" = 1000,
#   "Megabases"= 1,
#   "Q30_Base_Fraction" = 0.05,
#   "Coverage_Depth" = 5,
#   "n_Contigs_Length_0" = 5,
#   "n_Contigs_Length_1000" = 5,
#   "N50" = 1000,
#   "Read_Fraction_Majority_Species" = 0.05,
#   "Read_Fraction_Majority_Genus"   = 0.05,  
#   "Contig_Fraction_Majority_Species" = 0.05,
#   "Contig_fraction_Majority_Genus" = 0.05,
#   "Contaminating_SNVs" = 1,
#   "Single_Copy_Orthologs" = 0.05,
#   "Duplicated_Orthologs" = 0.05,
#   "MLST_Loci_Multiple_Alleles" = 0,
#   "MLST_Loci_Missing" = 0,
#   "n_Full_Genes" = 5,                    
#   "n_Partial_genes" = 5,
#   "Fraction_Genes_Recovered" = 0.02,
#   "Reference_Coverage" = 0.02,
#   "Duplication_Ratio" = 0.02,
#   "GC" = 1,
#   "Total_Length" = 1000,       
#   "Reference_Length" = 10000,
#   "Reference_Similarity" = 0.02,
#   "Fraction_Mapped_Reads" = 0.02,           
#   "Insert_Size"  = 20
# )
# 
# 
# # COLUMNS THAT MUST BE EQUAL
# equal_cols <- c(
#   "Sample_Name",
#   "QC_Vote",
#   "Species",
#   "Contamination_Status",
#   "MLST_Schema"
# )
# 
# diff_notolerance <- c(
#   "MLST_ST",
#   "QC_Fail",
#   "QC_Warning",
#   "QC_NotDetermined")
# 
# # unchecked columns
# unchecked_cols <- data.frame(
#   "run_name" = "",
#   "version" = "",
#   "timestamp" = "",
#   "Fasta_File" = "",
#   "analysis_type" = "Check"
# )
