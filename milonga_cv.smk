# AQUAMIS-CV snakemake workflow

# input
# date-version-tag

# output
# milonga results
# milonga-CV report

import os
import re
import pandas as pd
import shutil
from datetime import datetime
shell.executable("bash")


# definitions ---------------------------------------------------------------


# Set snakemake main workdir variable
workdir: config["workdir"] # LATER

#workdir: "/cephfs/abteilung4/Projects_NGS/Assembly_pipeline/continuous_integration"

# parameters:
#AQUAMIS_REPO = "/home/DenekeC/Snakefiles/milonga"
#SAMPLES = "/cephfs/abteilung4/Projects_NGS/Assembly_pipeline/continuous_integration/testdata/samples.tsv"

MILONGA_REPO = config["milonga_repo"]
SAMPLES = config["samples"]
fixed_date = config["fixed_date"]
tag = config["tag"]
#fix_date = True
#fixed_date = "20210816"
#fixed_date = ""


# Get date
#if fix_date:

if fixed_date != "":
    DATE = str(fixed_date)
    print("Using fixed date: " + DATE)
#if config["fixed_date"] != "":
#    DATE = config["fixed_date"]
else:
    DATE = datetime.today().strftime('%Y%m%d')
    print("Using current date: " + DATE)


# get Version
pattern = re.compile(r'(?<=version = ")(.*)(?=")')
with open(os.path.abspath(os.path.join(MILONGA_REPO, 'milonga.py')), "r") as wrapper:
    VERSION = [x for x in [re.search(pattern, line) for line in wrapper] if x is not None][0].group()
# stream = os.popen('grep "^version = " /home/DenekeC/Snakefiles/milonga/milonga.py | cut -f 2 -d "=" | tr -d \' \' | tr -d \'"\'')
# VERSION = stream.read().rstrip("\n")
print("Version is " + VERSION)


# Define outdir
if tag != "":
    OUTDIR = DATE + "_v" + VERSION + "_" + tag
else:
    OUTDIR = DATE + "_v" + VERSION
#OUTDIR_REPORTS="$BASEDIR/milonga_CV_Reports"
#NAME=${DATE}_v${VERSION}

# target rule --------------------------------------------------

rule all:
    input:
        OUTDIR + "/reports/summary_report.html", # milonga report
        OUTDIR + "/reports/milonga_CV_report.html",
        "milonga_CV_reports/" + OUTDIR + ".html"

rule all_cv:
    input:
        OUTDIR + "/reports/milonga_CV_report.html",
        "milonga_CV_reports/" + OUTDIR + ".html"

rule all_milonga:
    input:
        OUTDIR + "/reports/assembly_report.html", # milonga report

# --------------------------------------------------

rule run_milonga:
    input: SAMPLES
    output: OUTDIR + "/reports/summary_report.html"
    message: "running milonga"
    threads: 100
    #conda:
    log: "logs/milonga.log"
    params:
        path2milonga = MILONGA_REPO,
        outdir = OUTDIR,
        version = VERSION
    shell:
        """
        echo "{params.path2milonga}/milonga.py -l {input} -d {params.outdir} --threads {threads}"
        {params.path2milonga}/milonga.py -l {input} -d {params.outdir} --threads {threads}
        """

rule create_report:
    input: OUTDIR + "/reports/summary_report.html"
    output: OUTDIR + "/reports/milonga_CV_report.html"
    message: "Creating milonga-cv report" 
    #conda:
    log: "logs/creating_report.log"
    params:
        outdir = OUTDIR,
        name = OUTDIR,
        version = VERSION,
        date = DATE,
        reference_version = config["reference_version"]
    script:
        "scripts/milonga_CV_report.Rmd"

#echo "$path2r/Rscript -e \"rmarkdown::render('/cephfs/abteilung4/Projects_NGS/Assembly_pipeline/continuous_integration/scripts/compare_milonga_report.Rmd',intermediates_dir='$HOME/tmp/',params=list(name=\"$NAME\"),output_file=$output_file)\""
#$path2r/Rscript -e "rmarkdown::render('/cephfs/abteilung4/Projects_NGS/Assembly_pipeline/continuous_integration/scripts/compare_milonga_report.Rmd',intermediates_dir='$HOME/tmp/',params=list(name=\"$NAME\"),output_file=$output_file)"


rule copy_report:
    input: OUTDIR + "/reports/milonga_CV_report.html"
    output: "milonga_CV_reports/" + OUTDIR + ".html"
    message: "copying aquamic_cv report"
    #params:
    shell:
        """
        echo "cp {input} {output}"
        cp {input} {output}
        """
        
#echo "cp $output_file_simple $OUTDIR_REPORTS/MILONGA-CV_$NAME.html"
#cp $output_file_simple $OUTDIR_REPORTS/MILONGA-CV_$NAME.html

